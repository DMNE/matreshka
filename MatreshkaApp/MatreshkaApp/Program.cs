﻿using System;

namespace MatreshkaApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Matreshka matreshka = new Matreshka();

            Random rnd = new Random();
            
            matreshka.MatreshkaSize = rnd.Next(0, 10);
            Console.WriteLine(matreshka.MatreshkaSize);

            matreshka.MatreshkaSize = rnd.Next(0, 50);
            Console.WriteLine(matreshka.MatreshkaSize);

            matreshka.MatreshkaSize = rnd.Next(-100, 100);
            Console.WriteLine(matreshka.MatreshkaSize);

            matreshka.MatreshkaSize = rnd.Next(-50, 50);
            Console.WriteLine(matreshka.MatreshkaSize);
            
            matreshka.MatreshkaSize = 10;
            Console.WriteLine(matreshka.MatreshkaSize);
            matreshka.matreshka.matreshka.matreshka.matreshka.matreshka.matreshka.MatreshkaChild();

            Console.ReadKey();
        }
    }
}
