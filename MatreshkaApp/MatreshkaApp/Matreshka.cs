﻿using System;

namespace MatreshkaApp
{
    class Matreshka
    {
        private int _matreshkaSize;

        private readonly Matreshka _childMatreshka;
        
        public Matreshka(Matreshka childMatreshka)
        {
            _childMatreshka = childMatreshka;
        }

        public void MatreshkaParent()
        {
            
        }

        public void MatreshkaChild()
        {
            
        }

        public int MatreshkaSize
        {
            get 
            {
                Console.WriteLine($"Твоя матрешка имеет размер: {_matreshkaSize}");

                GetInnerMatreshka(_matreshkaSize);

                return _matreshkaSize;
            }

            set 
            { 
                if (0 < value && value <= 10) 
                { 
                    _matreshkaSize = value; 
                } 

                else 
                { 
                    Console.WriteLine($"Размер вне пределов допустимых значений. Введено: {value}"); //exception
                }
            }
        }

        private void GetInnerMatreshka(int size)
        {
            if (size > 0)
            {
                Console.WriteLine($"А в ней: {size - 1}");

                GetInnerMatreshka(size - 1);
            }

            else _matreshkaSize = size;
        }
    }
}
